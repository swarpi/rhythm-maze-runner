using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridCell : MonoBehaviour
{
    private int posX;
    private int posY;

    // Saves a reference to the gameObject thats gets placed on this cell
    public GameObject objectInThisGridSpace = null;

    //Saves if the grid space is occupied or not
    public bool isOccupied = false;

    //Set the posistion of this grid cell on the grid
    public void SePosition(int x, int y)
    {
        posX = x;
        posY = y;
    }

    //Get the postion of this grid space on the grid

    public Vector3Int GetPosition()
    {
        return new Vector3Int(posX,0, posY);
    }
    
}
