using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameGrid : MonoBehaviour
{

    private int height = 10;
    private int width = 10;
    private float gridSpaceSize = 1.5f;

    [SerializeField] private GameObject gridCellPrefab;
    private GameObject[,] gameGrid;

    void Start()
    {
        CreateGrid();
    }
    //Create Grid when the game starts
     private void CreateGrid()
    {
        gameGrid = new GameObject[height, width];

        if(gridCellPrefab == null)
        {
            Debug.LogError("ERROR: Grid Cell Prefab on the Game grid is nicht assigned");
        }

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                //Create a new GridSpace objerct for each cell
                gameGrid[x, y] = Instantiate(gridCellPrefab,new Vector3(x* gridSpaceSize, 0,y* gridSpaceSize),Quaternion.identity);
                gameGrid[x, y].GetComponent<GridCell>().SePosition(x, y);
                gameGrid[x, y].transform.parent = transform;
                gameGrid[x, y].gameObject.name = "Grid Space( X: " + x.ToString() + " , Y: " + y.ToString() + ")";
            }
        }
    }

    // Gets the grid position from world position
    public Vector3 GetGridPosFromWorld(Vector3 worldPosition)
    {
        int x = Mathf.FloorToInt(worldPosition.x / gridSpaceSize);
        int z = Mathf.FloorToInt(worldPosition.z / gridSpaceSize);

        x = Mathf.Clamp(x, 0, width);
        z = Mathf.Clamp(x, 0, height);

        return new Vector3(x, 0, z);
    }

    //Gets the world position of a grid position
    public Vector3 GetWorldPosFromGridPos(Vector3 gridPos)
    {
        float x = gridPos.x * gridSpaceSize;
        float z = gridPos.z * gridSpaceSize;

        return new Vector3(x, 0, z);
    }
}
