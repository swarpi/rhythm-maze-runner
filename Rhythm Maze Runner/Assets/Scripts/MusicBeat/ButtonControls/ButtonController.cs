using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
// Controller Class for a single Button
// deals with logic of pressing button
// changes appeareance of the button
// notifies that the button has been pressed
public class ButtonController : MonoBehaviour
{
    public Image buttonImage;
    public Color defaultColor;
    public Color pressedColor;

    public KeyCode keyToPress;
    // Start is called before the first frame update
    void Start()
    {
        this.buttonImage = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(keyToPress))
        {
            ButtonPressed();
        }
        if(Input.GetKeyUp(keyToPress))
        {
            ButtonReleased();
        }
    }

    void ButtonPressed()
    {
        buttonImage.color = pressedColor;
    }

    void ButtonReleased()
    {
        buttonImage.color = defaultColor;
    }
}
