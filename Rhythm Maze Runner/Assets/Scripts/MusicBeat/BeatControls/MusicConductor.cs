using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// this class will be in control for the music timing
// track the song position, and control any other synced actions
// class was created with the help of an online tutorial:
// https://www.gamedeveloper.com/audio/coding-to-the-beat---under-the-hood-of-a-rhythm-game-in-unity
public class MusicConductor : MonoBehaviour
{
    //Song beats per minute
    //This is determined by the song you're trying to sync up to
    public float songBpm;

    //The number of seconds for each song beat
    public float secPerBeat;

    //Current song position, in seconds
    public float songPosition;

    //Current song position, in beats
    public float songPositionInBeats;

    //How many seconds have passed since the song started
    public float dspSongTime;

    //an AudioSource attached to this GameObject that will play the music.
    public AudioSource musicSource;

    // offbeat if the music doenst start immediatly when the music is played
    public float firstBeatOffset;

    // position of the beat based on its starting
    public float normalizedBeatPosition;

    // Start is called before the first frame update
    void Start()
    {
        //Load the AudioSource attached to the Conductor GameObject
        musicSource = GetComponent<AudioSource>();

        //Calculate the number of seconds in each beat
        secPerBeat = 60f / songBpm;


    }

    // Update is called once per frame
    void Update()
    {
        if (BeatManager.Instance.hasStarted)
        {


            //determine how many seconds since the song started
            songPosition = (float)(AudioSettings.dspTime - dspSongTime) - firstBeatOffset;

            //determine how many beats since the song started
            songPositionInBeats = songPosition / secPerBeat;

            // normalize the beat position of 0 to 1
            // TODO need to refactor this later to adjust it when the beat position is not always the same
            normalizedBeatPosition = songPositionInBeats - Mathf.Floor(songPositionInBeats);
        }

    }

    public void StartMusic()
    {
        //Record the time when the music starts
        dspSongTime = (float)AudioSettings.dspTime;
        musicSource.Play();
    }
}
