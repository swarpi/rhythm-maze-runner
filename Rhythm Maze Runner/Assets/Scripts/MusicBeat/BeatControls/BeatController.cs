using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
// Controls one single beat
// maybe want to refactor this a bit so instead of having to beat gameobject just have one that controls 2 beats
public class BeatController : MonoBehaviour
{
    public Stopwatch zeit = new Stopwatch();

    float distanceToHitField;

    GameObject leftBeat;
    GameObject rightBeat;
    BeatManager man = BeatManager.Instance;
    int beatPosition;

    public void Start()
    {
        zeit.Start();
        leftBeat = transform.GetChild(0).gameObject;
        rightBeat = transform.GetChild(1).gameObject;

    }
    // use this later if we want to initialize with a beat array, because with the metronom now the beat position of all beats is spaced the same
    public void Initialize()
    {

    }
    // Update is called once per frame
    void Update()
    {
        MoveBeat();
        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    UnityEngine.Debug.Log("pressing space");
        //    if (BeatHit())
        //    {
        //        BeatManager.Instance.BeatGotHit();
        //    }
        //    else
        //        UnityEngine.Debug.Log("miss");
        //}
    }

    void MoveBeat()
    {
        // for now normalizedbeat position is fine, but if the beats are not always at the same position need an array to store the beat positions
        // useful links:
        // https://shinerightstudio.com/posts/music-syncing-in-rhythm-games/ // also check here out the git project
        // https://github.com/Gkxd/Rhythmify/tree/master/Assets/Rhythmify_Scripts/rhythmObject // could be helpful
        // https://medium.com/@thibautdumont/rhythm-game-with-unity3d-achieve-latency-free-sync-android-and-other-platforms-c05fa8e2718b // good basic tutorial
        // https://www.gamedeveloper.com/audio/coding-to-the-beat---under-the-hood-of-a-rhythm-game-in-unity // good basic tutorial

        distanceToHitField = leftBeat.transform.position.x - BeatManager.Instance.hitFieldPosition.position.x;

        // lerp the start the position with the final position and a bit ( cause we want to move a bit further than just touching the hitField)
        // and do it based on the position of the beat in its intverall
        // (e.g. 27th beat is between 27 secs and 28 secs, so and by normalizing it should be 1 when the next beat comes
        leftBeat.transform.position = Vector2.Lerp(man.leftSpawn.position, man.hitFieldPosition.position + new Vector3(10,0,0), man.musicConductor.normalizedBeatPosition);
        if (leftBeat.transform.localPosition.x >= 400)
        {
            BeatManager.Instance.inRange = true;
        }
        rightBeat.transform.position = Vector2.Lerp(man.rightSpawn.position, man.hitFieldPosition.position + new Vector3(10, 0, 0), man.musicConductor.normalizedBeatPosition);
        if (distanceToHitField >= -10)
        {
            BeatManager.Instance.inRange = false;

            TimeSpan ts = zeit.Elapsed;
            zeit.Stop();
            man.printTimer(ts.Milliseconds); // figure if there is a delay, if yes adjust the spawn rate timing so it matches the beat
            
            Destroy(this.gameObject);
        }
    }
    // if we want to distinguish between good and bad jumps just change the return to an enum
    // change this to make clicking not depending on visual but rather on the sound position and beat
    public bool BeatHit()
    {
        UnityEngine.Debug.Log(distanceToHitField.ToString());
        if (distanceToHitField >= -50) return true;

        return false;
    }
}
