using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Class that takes care of spawning the beat and checks if the beat was hit or not

public class BeatManager : MonoBehaviour
{
    #region Singleton
    private static BeatManager _instance;
    public static BeatManager Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    #endregion

    public bool inRange;
    public float timer = 0f;
    public bool hasStarted;

    // fields for the beat spawn cooldown
    public float nextBeat;

    public MusicConductor musicConductor;

    public GameObject beatGameObject;
    public Transform leftSpawn;
    public Transform rightSpawn;
    public Transform spawnPoint;
    public Transform hitFieldPosition;
    public float distanceFromSpawnToHitFieldPosition;

    public GameObject beatSensor;

    public int targetFrameRate = 60;    // for testing, limit fps

    public System.Diagnostics.Stopwatch zeit = new System.Diagnostics.Stopwatch();

    public void printTimer(int time)
    {
        Debug.Log("it took " + time + " milliseconds");     // TODO at the moment beat moving a bit to fast with 120Bpm and 60 fps should be 500 Milliseconds
    }

    public void Start()
    {
        distanceFromSpawnToHitFieldPosition = hitFieldPosition.position.x - leftSpawn.position.x;
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = targetFrameRate;        
    }

    public void Update()
    {

        // space key to start the game
        // if game hasnt started leave here
        if (!hasStarted)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                musicConductor.StartMusic();
                hasStarted = true;
                return;
                //zeit.Start();
            }
        }
        // spawn new beat every nextBeat interval
        // https://www.reddit.com/r/gamedev/comments/4chrz7/how_to_sync_music_in_a_rhythm_game/ // insipired by first comment
        if (musicConductor.songPosition > nextBeat + musicConductor.secPerBeat)
        {
            nextBeat += musicConductor.secPerBeat;
            SpawnBeat();
        }
        //Debug.Log("normalized beat position " + musicConductor.normalizedBeatPosition);
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("pressing space");
            if (BeatHit())
            {
                BeatGotHit();
            }
            else
                Debug.Log("miss");
        }
    }
    // if we want to distinguish between good and bad jumps just change the return to an enum
    // change this to make clicking not depending on visual but rather on the sound position and beat
    public bool BeatHit()
    {
        Debug.Log(musicConductor.normalizedBeatPosition);
        // first condition is to include to early strikes
        // second condition is to include to late strikes
        if (1 - musicConductor.normalizedBeatPosition < 0.1 || 1 - musicConductor.normalizedBeatPosition > 0.9)
            return true;
        return false;
    }

    void SpawnBeat()
    {
        GameObject.Instantiate(beatGameObject, spawnPoint);
    }

    public void BeatGotHit()
    {
        Debug.Log("Hit");
    }
}
